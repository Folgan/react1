var steelList = [];
var curSt = 0;
var currentChoice;
var currentName;
var arrayTmp = [];

function changeTable() {
var	Table	=	React.createClass({
	render:	function()	{
		var cur = curSt;
		var otzh = arrayTmp[cur].otzhig;
		var zak = arrayTmp[cur].zakalka;
		var lng = arrayTmp[cur].otpuskTmp.length;
		var otpTmp = arrayTmp[cur].otpuskTmp;
		var tmp1	= otpTmp.map(function(item,	index)	{
						return	(
							<dd key={index}>{otpTmp[index]}</dd>
						)
					});
		var otpTv = arrayTmp[cur].otpuskTv;
		var tmp2	= otpTv.map(function(item,	index)	{
						return	(
							<dd key={index}>{otpTv[index]}</dd>
						)
					});
		return	(
			<tr>
				<td width="25%">{otzh}</td><td width="25%">{zak}</td>
				<td width ="25%">{tmp1}</td><td>{tmp2}</td>
			</tr>
		);
	}
});

var	Context	=	React.createClass({

		render:	function()	{
			var cur = curSt;
			var cont = arrayTmp[cur].content;
				return	(
					<p>{cont}</p>
				);
		}
});

	ReactDOM.render(
		<Table	/>,
		document.getElementById('table')
	);

	ReactDOM.render(
		<Context	/>,
		document.getElementById('content')
	);
};

$.getJSON("json/list.json")
	 		.done(function( json ) {

	 		// ---%%%%%% - сортировщик 
	  			var st = json;
	  			var arr = [];
	  				for (var i = 0;i<st.length;i++) {
	  					arr.push(st[i].name);
	  				};
	  			var arr2 = arr.sort();
	  			var arr3 = [];
	  				for (var z=0;z<arr2.length;z++) {
	  					for (var s=0;s<arr2.length;s++) {
	  						if (arr2[z] == st[s].name) {
	  							arr3.push(st[s]);
	  						};
	  					};
	  				};
	  			steelList = arr3;
	  			arrayTmp = arr3
	  		// ---%%%%%%

	  			console.log('JSON data loaded')
	  			whenDataLoaded();
	  	})
	 		.fail(function(){
	 			console.log('JSON data unloaded')
		});	

function whenDataLoaded() {

	var	Count	=	React.createClass({
		render:	function()	{
			
			var cnt = steelList.length;
				return (
					<p>Всего сталей в базе: <b>{cnt}</b></p>
				);
		}
	});

	var	FilterCount	=	React.createClass({
		render:	function()	{
			var fcnt = arrayTmp.length;
			var clr;
				
					if (fcnt == steelList.length) {
						clr = "clrBlck";
					}
						else {
							clr = "clrRed"
						};
				
				return (
					<p>Cталей отфильтровано: <b className={clr}>{fcnt}</b></p>
				);
		}
	});

	var	List	=	React.createClass({
	
		readmoreClick:	function(e)	{
				
				if (currentChoice) {
					for(var i=0;i<steelList.length;i++){
						var t = "#"+i;
						$(t).removeAttr('style');
					};
				};

				e.preventDefault();
					this.setState({currentSteel:	e.target.id});
					curSt = e.target.id;
						$('#nameSteel h2').text(arrayTmp[curSt].name);
						changeTable();
							currentChoice = "#"+curSt;
							currentName = arrayTmp[curSt].name;
							$(currentChoice).attr('style', 'color: #FF0000; font-weight:bold; font-size: 18px;');
		},

		render:	function()	{

				var mach = $('#list').attr('class');
				

				var showList = function() {
					if (!mach) {
						return steelList;
					}
					else {
						var regxp = new RegExp(mach);
						var arrTmp = [];
						for (var i=0;i<steelList.length;i++) {
							var z = steelList[i].name;
							if (z.search(regxp) >= 0) {
								arrTmp.push(steelList[i]);
							};
						};
					};
					return arrTmp;
				};

				var currentSteel = showList();
					arrayTmp = currentSteel;
					var	listOfSteels	=	currentSteel.map(function(item,	index)	{
							return	(
									<li key={index}><a id={index} href="#">{item.name}</a></li>
							)
					})
				return	(
					<ul onClick={this.readmoreClick}>
						{listOfSteels}
					</ul>
				);
		}
});

function rendList() {
	ReactDOM.render(
		<List	/>,
		document.getElementById('list')
	);
};

	function rendFilterCount() {
		if (steelList.length == arrayTmp.length) {rendFilterCountUnexist();}
			else if (arrayTmp.length == 0) {rendFilterCountNull();}
				else rendFilterCountExist();
	};
			function rendFilterCountExist() {
				ReactDOM.render(
					<FilterCount	/>,
					document.getElementById('filtercount')
				);
			};
					function rendFilterCountNull() {
						ReactDOM.render(
							<p className="clrRed">Совпадений не найдено.</p>,
							document.getElementById('filtercount')
						);
					};
							function rendFilterCountUnexist() {
								ReactDOM.render(
									<p>&nbsp;</p>,
									document.getElementById('filtercount')
								);
							};
rendList();
rendFilterCount();

var	Filt	=	React.createClass({
		render:	function()	{
			function dataSearch(e) {
				var mach = e.target.value.toUpperCase();
				$('#list').attr('class', mach);
				rendList();
				listHover();
				rendFilterCount();

				for (var i=0;i<arrayTmp.length;i++) {
					if (arrayTmp[i].name == currentName) {
					var v = "#"+i;
					$(v).attr('style', 'color: #FF0000; font-weight:bold; font-size: 18px;');
					}
				};
			}
			return	(
				<input maxlength="10" placeholder="Фильтр" onChange={dataSearch}></input>

			);
		}
});

ReactDOM.render(
		<Filt />,
		document.getElementById('filt')
);

ReactDOM.render(
		<Count	/>,
		document.getElementById('datacount')
);

// --- %%% Прокрутка списка сталей
function listHover() {
	$("#list").css("overflow", "hidden").wrapInner("<div id='mover' />");
		var $el,
		    speed = arrayTmp.length/11+10,
		    cur = -1,
		    items = $("#list a"),
		    max = items.length - 10;
		items
		.each(function(i) {
			$(this).attr("data-pos", i);
		})
	.hover(function() {
		$el = $(this);
		$el.addClass("hover");
		$("#mover").css("top", -($el.data("pos") * speed - 0));
		cur = $el.data("pos");
	}, function() {
		$(this).removeClass("hover");
	});
};
// --- %%%
	listHover();
};